#!/usr/bin/env python3 
import sys

def iso(str1, str2):
  d = {}
  if not len(str1) == len(str2):
    return False

  for index, i  in enumerate(str1):
    if str1[index] not in d:
       d[i] = str2[index] 
    else:
       if not d[i] == str2[index]:
         return False
  
  if len(set(d.values())) < len(d):
    return False

  return True

if __name__ =='__main__':
  for line in sys.stdin:
    str1, str2 = line.split()
    if iso(str1, str2):
      print("Isomorphic")
    else:
      print("Not Isomorphic")
