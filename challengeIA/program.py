#!/usr/bin/env python3

import sys

def sunset_watch(buildings):
    count = 0
    for index, ht in enumerate(buildings):
        if index == len(buildings) - 1:
            count += 1
            continue
        sunrise = True
        for ht2 in buildings[index + 1:]:
            if ht2 >= ht:
                sunrise = False
        if sunrise:
            count += 1
    return count


if __name__ == '__main__':
    for line in sys.stdin:
        buildings = list(map(int, line.split()))
        print(sunset_watch(buildings))


