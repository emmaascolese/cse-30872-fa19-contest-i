#!/usr/bin/env python3

import sys

SCORING_PLAYS = (2, 3, 7)

ALL_COMBINATIONS = {}

def score_combos(path, score):
    if score == 0:
        yield path

    if score < 0:
        return

    combos = []
    for i in SCORING_PLAYS:
        yield from score_combos(path + str(i), score - i)
        '''if ALL_COMBINATIONS[score - i]:
            #combos.append([path + x for x in ALL_COMBINATIONS[score - i]])
            #yield from [path + x for x in ALL_COMBINATIONS[score - i]]
        else:
            combos.append
            #yield from score_combos(path + str(i), score - i)'''

def remove_duplicates(lst):
    lst = list(map(sorted, lst))
    new_list = []
    
    lst.sort()

    if lst:
        new_list.append(lst[0])
    
    for index, i in enumerate(lst[1:], 1):
        if not i == lst[index - 1]:
            new_list.append(i)
    return new_list

if __name__ == '__main__':
    for line in sys.stdin:
        score = int(line)
        combos = list(score_combos('', score))
        combos = remove_duplicates(combos)
        num = len(combos)

        output = 'There {} {} way{} to achieve a score of {}:'

        if num == 1:
            output = output.format('is', num, '', score)
        else:
            output = output.format('are', num, 's', score)

        print(output)
        [print(' '.join(list(map(str,x)))) for x in combos]

            
