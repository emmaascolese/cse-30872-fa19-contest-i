#!/usr/bin/env python3

import sys

def perm(numbers, swaps, lst):
    head = 0
    while swaps > 0:

        # case where the list is already optimized    
        if numbers == 0:
            break

        ind = lst.index(numbers)

        # case where target element is already in optimal location
        if ind == head:
            head += 1
            numbers -= 1
            continue

        lst[ind] = lst[head]
        lst[head] = numbers

        numbers -= 1
        head += 1
        swaps -= 1

    return lst

       

if __name__ == "__main__":
    while True:
        line1 = sys.stdin.readline()
        if not line1:
            break
        numbers, swaps = line1.split()
        numbers = int(numbers)
        swaps = int(swaps)
        line2 = sys.stdin.readline()
        lst = list(map(int, line2.split()))

        answer = perm(numbers, swaps, lst)
        answer = list(map(str, answer))
        print(' '.join(answer))


