#!/usr/bin/env python3

import sys

def maximize_bitcoin(servers):
    return adjacent_checker(servers, 0, {})


def adjacent_checker(servers, index, d):
    if len(servers) == 0:
        d[index] = 0
        return 0
    if len(servers) == 1:
        d[index] = servers[0]
        return servers[0]

    if index in d:
        return d[index]
    
    with_c1 = servers[0] + adjacent_checker(servers[2:], index + 2, d)
    without_c1 = adjacent_checker(servers[1:], index + 1, d)
    
    d[index] = max(with_c1, without_c1)
    return max(with_c1, without_c1)



if __name__ == '__main__':
    for line in sys.stdin:
        servers = list(map(int, line.split()))
        bitcoin = maximize_bitcoin(servers)
        print(bitcoin)
