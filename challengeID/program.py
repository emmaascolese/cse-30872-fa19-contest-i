#!/usr/bin/env python3

import sys

def addition_to_zero(numbers):
    numbers.sort()

    answers = []

    for i, val_i in enumerate(numbers):
        if not i == 0 and numbers[i - 1] == val_i:
            continue
        for j, val_j in enumerate(numbers[i+1:], i+1):
            if not j == i + 1 and numbers[j - 1] == val_j:
                continue
            for k, val_k in enumerate(numbers[j+1:], j+1):
                if not k == j + 1 and numbers[k - 1] == val_k:
                    continue
                if val_i + val_j + val_k == 0:
                    answers.append([val_i, val_j, val_k])
    return answers

if __name__ == '__main__':
    for index, line in enumerate(sys.stdin):
        if index: 
            print()
        lst = list(map(int, line.split()))
        answers = addition_to_zero(lst)
        [print(' + '.join(map(str, answer))) for answer in answers]
